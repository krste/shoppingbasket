# Shopping basket

This project is created as effort to create class library for shopping basket 
that can be implemented on web pages. Solution contains three project:

* **ShoppingBasket** - class library
* **ShoppingBasket.DemoWeb** - Use case for ShoppingBasket class library 
* **ShoppingBasket.Tests** - Unit tests for ShoppingBasket class library

No additional optimization (at this point) is required because current 
implementation meets current requirements.