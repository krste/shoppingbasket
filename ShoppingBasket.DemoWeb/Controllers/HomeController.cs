﻿using System.Web.Mvc;
using ShoppingBasket.DemoWeb.Models;
using ShoppingBasket.DemoWeb.Services;
using ShoppingBasket.Models;
using ShoppingBasket.Services;
using ShoppingBasket.Services.Interfaces;

namespace ShoppingBasket.DemoWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly IShoppingService _service;

        public HomeController() : this(new SessionShoppingService()) { }

        public HomeController(IShoppingService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            return View(_service);
        }

        public ActionResult AddDiscounts()
        {
            _service.RemoveAllDiscountServices();
            _service.AddDiscountService(new BreadDiscountService());
            _service.AddDiscountService(new MilkDisountService());

            return RedirectToAction("Index");
        }

        public ActionResult RemoveDiscounts()
        {
            _service.RemoveAllDiscountServices();

            return RedirectToAction("Index");
        }

        public ActionResult AddButter(int quantity = 1)
        {
            _service.AddProduct(new BasketItem(new Butter(), quantity));
            return RedirectToAction("Index");
        }

        public ActionResult AddMilk(int quantity = 1)
        {
            _service.AddProduct(new BasketItem(new Milk(), quantity));
            return RedirectToAction("Index");
        }

        public ActionResult AddBread(int quantity = 1)
        {
            _service.AddProduct(new BasketItem(new Bread(), quantity));
            return RedirectToAction("Index");
        }

        public ActionResult RemoveButter(int quantity = 1)
        {
            _service.RemoveProduct(new BasketItem(new Butter(), quantity));
            return RedirectToAction("Index");
        }

        public ActionResult RemoveMilk(int quantity = 1)
        {
            _service.RemoveProduct(new BasketItem(new Milk(), quantity));
            return RedirectToAction("Index");
        }

        public ActionResult RemoveBread(int quantity = 1)
        {
            _service.RemoveProduct(new BasketItem(new Bread(), quantity));
            return RedirectToAction("Index");
        }

        public ActionResult AddNewDiscounts()
        {
            _service.RemoveAllDiscountServices();
            _service.AddDiscountService(new TotalDisountService(20, new decimal(0.2)));
            return RedirectToAction("Index");
        }

        public ActionResult RemoveNewDiscounts()
        {
            _service.RemoveAllDiscountServices();
            return RedirectToAction("Index");
        }
    }
}