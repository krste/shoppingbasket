﻿using ShoppingBasket.Models.Interfaces;

namespace ShoppingBasket.DemoWeb.Models
{
    public class Milk : IProduct
    {
        public int Id => 2;
        public decimal Price => new decimal(1.15);
        public string Name => "Milk";
    }
}