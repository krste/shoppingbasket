﻿using ShoppingBasket.Models.Interfaces;

namespace ShoppingBasket.DemoWeb.Models
{
    public class Bread : IProduct
    {
        public int Id => 3;
        public decimal Price => 1;
        public string Name => "Bread";
    }
}