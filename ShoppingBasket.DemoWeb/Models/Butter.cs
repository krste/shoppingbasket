﻿using ShoppingBasket.Models.Interfaces;

namespace ShoppingBasket.DemoWeb.Models
{
    public class Butter : IProduct
    {
        public int Id => 1;
        public decimal Price => new decimal(0.8);
        public string Name => "Butter";
    }
}