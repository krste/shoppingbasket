﻿using ShoppingBasket.DemoWeb.Models;
using ShoppingBasket.Services;

namespace ShoppingBasket.DemoWeb.Services
{
    public class MilkDisountService : ItemDiscountService
    {
        public MilkDisountService() : base(2, new Milk(), 3, new Milk(), 1) { }
    }
}