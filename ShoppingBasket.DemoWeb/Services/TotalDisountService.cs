﻿using System.Collections.Generic;
using System.Linq;
using ShoppingBasket.Models;
using ShoppingBasket.Models.Interfaces;
using ShoppingBasket.Services.Interfaces;

namespace ShoppingBasket.DemoWeb.Services
{
    public class TotalDisountService : IDiscountService
    {
        private readonly int _discountAmount;
        private readonly decimal _discountShare;

        public TotalDisountService(int discountAmount, decimal discountShare)
        {
            _discountAmount = discountAmount;
            _discountShare = discountShare;
        }

        public int Id => 3;
        public IDiscount ProcessDiscount(ICollection<IBasketItem> items)
        {
            var totalAmount = items.Sum(i => i.TotalPrice);

            return totalAmount >= _discountAmount ? new Discount(new Product(-1, totalAmount, "Total"), 1, _discountShare) : null;
        }
    }
}