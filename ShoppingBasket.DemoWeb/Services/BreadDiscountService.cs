﻿using ShoppingBasket.DemoWeb.Models;
using ShoppingBasket.Services;

namespace ShoppingBasket.DemoWeb.Services
{
    public class BreadDiscountService : ItemDiscountService
    {
        public BreadDiscountService() : base(1, new Butter(), 2, new Bread(), new decimal(0.5)) { }
    }
}