﻿using ShoppingBasket.Services.Interfaces;

namespace ShoppingBasket.Logger
{
    /// <summary>
    /// Logger used inside <see cref="IShoppingService"/>
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Info level of logging
        /// </summary>
        /// <param name="message">Object that will be logged</param>
        void Info(object message);

        /// <summary>
        /// Warning level of logging
        /// </summary>
        /// <param name="message">Object that will be logged</param>
        void Warning(object message);

        /// <summary>
        /// Error level of logging
        /// </summary>
        /// <param name="message">Object that will be logged</param>
        void Error(object message);
    }
}