﻿using System;

namespace ShoppingBasket.Logger
{
    /// <inheritdoc />
    internal class ConsoleLogger : ILogger
    {
        public void Info(object message)
        {
            Console.WriteLine($"INFO: \n{message}");
        }

        public void Warning(object message)
        {
            Console.WriteLine($"WARNING: \n{message}");
        }

        public void Error(object message)
        {
            Console.WriteLine($"ERROR: \n{message}");
        }
    }
}