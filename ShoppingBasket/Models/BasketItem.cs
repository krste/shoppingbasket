﻿using ShoppingBasket.Models.Interfaces;

namespace ShoppingBasket.Models
{
    /// <inheritdoc />
    public class BasketItem : IBasketItem
    {
        /// <summary>
        /// Create new instance of <see cref="BasketItem"/>
        /// </summary>
        /// <param name="product">Object that implements <see cref="IProduct"/> interface</param>
        /// <param name="quantity">Number of items of this <see cref="Product"/></param>
        public BasketItem(IProduct product, int quantity = 1)
        {
            Product = product;
            Quantity = quantity;
        }

        public int Quantity { get; private set; }

        public decimal TotalPrice => Product.Price * Quantity;

        public IProduct Product { get; }

        public void SetQuantity(int quantity)
        {
            Quantity = (quantity > 0) ? quantity : 0;
        }
    }
}