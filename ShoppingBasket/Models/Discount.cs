﻿using ShoppingBasket.Models.Interfaces;

namespace ShoppingBasket.Models
{
    /// <inheritdoc />
    public class Discount : IDiscount
    {
        /// <summary>
        /// Create new instance of <see cref="Discount"/>
        /// </summary>
        /// <param name="product">Type of product</param>
        /// <param name="quantity">The number of products to which the discount relates</param>
        /// <param name="discountShare">Percentage displayed as decimal number (20% => 0.2)</param>
        public Discount(IProduct product, int quantity, decimal discountShare)
        {
            Product = product;
            Quantity = quantity;
            TotalDiscount = (product.Price * discountShare) * quantity;
        }

        public IProduct Product { get; }

        public int Quantity { get; }

        public decimal TotalDiscount { get; }
    }
}