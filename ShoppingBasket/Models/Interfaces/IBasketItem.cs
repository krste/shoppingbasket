﻿namespace ShoppingBasket.Models.Interfaces
{
    /// <summary>
    /// Group of products in basket
    /// </summary>
    public interface IBasketItem
    {
        /// <summary>
        /// Number of <see cref="IProduct"/> in basket
        /// </summary>
        int Quantity { get; }

        /// <summary>
        /// Type of <see cref="Product"/>
        /// </summary>
        IProduct Product { get; }

        /// <summary>
        /// Calculated total price for this <see cref="Product"/>
        /// </summary>
        decimal TotalPrice { get; }

        /// <summary>
        /// Method to update quantiti of this <see cref="Product"/>
        /// </summary>
        /// <param name="quantity"></param>
        void SetQuantity(int quantity);
    }
}