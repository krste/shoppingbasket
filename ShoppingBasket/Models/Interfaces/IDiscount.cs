﻿namespace ShoppingBasket.Models.Interfaces
{
    /// <summary>
    /// Discount 
    /// </summary>
    public interface IDiscount
    {
        /// <summary>
        /// Product that is discounted
        /// </summary>
        IProduct Product { get; }

        /// <summary>
        /// Number of <see cref="Product"/> that is discounted
        /// </summary>
        int Quantity { get; }

        /// <summary>
        /// Total amount of discount
        /// </summary>
        decimal TotalDiscount { get; }
    }
}