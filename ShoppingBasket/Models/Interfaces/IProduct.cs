﻿using ShoppingBasket.Services;

namespace ShoppingBasket.Models.Interfaces
{
    /// <summary>
    /// Interface for Product that <see cref="ShoppingService"/> can process
    /// Made this way so entity can inherit this interface and read product info directly from database
    /// </summary>
    public interface IProduct
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Price of one item of this product
        /// </summary>
        decimal Price { get; }

        /// <summary>
        /// User friendly name of product
        /// </summary>
        string Name { get; }
    }
}