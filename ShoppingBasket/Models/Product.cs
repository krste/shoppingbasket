﻿using ShoppingBasket.Models.Interfaces;

namespace ShoppingBasket.Models
{
    /// <inheritdoc />
    public class Product : IProduct
    {
        /// <summary>
        /// Create new instance of <see cref="Product"/>
        /// </summary>
        public Product(int id, decimal price, string name)
        {
            Id = id;
            Price = price;
            Name = name;
        }

        public int Id { get; }

        public decimal Price { get; }

        public string Name { get; }
    }
}