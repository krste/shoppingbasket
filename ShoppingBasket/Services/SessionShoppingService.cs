﻿using System.Collections.Generic;
using System.Web;
using ShoppingBasket.Logger;
using ShoppingBasket.Models.Interfaces;
using ShoppingBasket.Services.Interfaces;

namespace ShoppingBasket.Services
{
    /// <inheritdoc />
    public class SessionShoppingService : IShoppingService
    {
        private readonly ShoppingService _service;

        public SessionShoppingService(ILogger logger = null)
        {
            _service = (ShoppingService)HttpContext.Current.Session["ShoppingService"];

            if (_service == null)
            {
                _service = new ShoppingService(logger);
                SetCurrent();
            }
        }

        public void AddProduct(IBasketItem product)
        {
            _service.AddProduct(product);
            SetCurrent();
        }

        public void RemoveProduct(IBasketItem product)
        {
            _service.RemoveProduct(product);
            SetCurrent();
        }

        public ICollection<IBasketItem> GetProducts()
        {
            return _service.GetProducts();
        }

        public void AddDiscountService(IDiscountService discountService)
        {
            _service.AddDiscountService(discountService);
            SetCurrent();
        }

        public void RemoveDiscountService(int id)
        {
            _service.RemoveDiscountService(id);
            SetCurrent();
        }

        public void RemoveAllDiscountServices()
        {
            _service.RemoveAllDiscountServices();
            SetCurrent();
        }

        public ICollection<IDiscount> GetDiscounts()
        {
            return _service.GetDiscounts();
        }

        public decimal GetBasketBalance()
        {
            var balance = _service.GetBasketBalance();
            SetCurrent();
            return balance;
        }

        public override string ToString()
        {
            return _service.ToString();
        }

        private void SetCurrent()
        {
            HttpContext.Current.Session["ShoppingService"] = _service;
        }
    }
}