﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Ajax.Utilities;
using ShoppingBasket.Logger;
using ShoppingBasket.Models.Interfaces;
using ShoppingBasket.Services.Interfaces;

namespace ShoppingBasket.Services
{
    /// <inheritdoc />
    public class ShoppingService : IShoppingService
    {
        private readonly ILogger _logger;
        private decimal _backetSum;
        private readonly ICollection<IDiscountService> _discountServices = new List<IDiscountService>();
        private readonly ICollection<IBasketItem> _items = new List<IBasketItem>();
        private readonly ICollection<IDiscount> _discounts = new List<IDiscount>();

        /// <summary>
        /// Return new instance of <see cref="ShoppingService"/>
        /// </summary>
        /// <param name="logger"></param>
        public ShoppingService(ILogger logger = null)
        {
            _logger = logger ?? new ConsoleLogger();
        }

        public void AddProduct(IBasketItem product)
        {
            var bucketProduct = _items.FirstOrDefault(p => p.Product.Id == product.Product.Id);

            if (bucketProduct == null)
            {
                _items.Add(product);
            }
            else
            {
                bucketProduct.SetQuantity(bucketProduct.Quantity + product.Quantity);
            }

            CalculateBalance();
        }

        public void RemoveProduct(IBasketItem product)
        {
            var bucketProduct = _items.FirstOrDefault(p => p.Product.Id == product.Product.Id);

            if (bucketProduct == null)
            {
                return;
            }

            bucketProduct.SetQuantity(bucketProduct.Quantity - product.Quantity);

            if (bucketProduct.Quantity == 0)
            {
                _items.Remove(bucketProduct);
            }

            CalculateBalance();
        }

        public ICollection<IBasketItem> GetProducts()
        {
            return _items;
        }

        public void AddDiscountService(IDiscountService discountService)
        {
            var service = _discountServices.FirstOrDefault(ds => ds.Id == discountService.Id);
            if (service != null)
            {
                return;
            }

            _discountServices.Add(discountService);
            CalculateBalance();
        }

        public void RemoveDiscountService(int id)
        {
            var service = _discountServices.FirstOrDefault(ds => ds.Id == id);

            if (service == null)
            {
                return;
            }

            _discountServices.Remove(service);
            CalculateBalance();
        }

        public void RemoveAllDiscountServices()
        {
            _discountServices.Clear();
            CalculateBalance();
        }

        public ICollection<IDiscount> GetDiscounts()
        {
            return _discounts;
        }

        public decimal GetBasketBalance()
        {
            _logger.Info(ToString());

            return _backetSum;
        }

        /// <summary>
        /// Remove multiple items to backet
        /// </summary>
        /// <param name="products">List of <see cref="IBasketItem"/> that will be removed from backet</param>
        public void RemoveProducts(ICollection<IBasketItem> products)
        {
            products.ForEach(RemoveProduct);
        }

        /// <summary>
        /// Add multiple items to backet
        /// </summary>
        /// <param name="products">List of <see cref="IBasketItem"/> that will be added to backet</param>
        public void AddProducts(ICollection<IBasketItem> products)
        {
            products.ForEach(AddProduct);
        }

        /// <summary>
        /// Just for demo purpose
        /// </summary>
        /// <returns>Content of bucket as string</returns>
        public override string ToString()
        {
            var toString = new StringBuilder();

            toString.AppendLine(new string('*', 14) + " Basket balance " + new string('*', 14));
            toString.AppendLine("  Product | Quantity |  Price   |  Total   |");
            toString.AppendLine(new string('-', 44));
            _items.ForEach(item =>
            {
                toString.AppendLine($"{item.Product.Name,10}|{item.Quantity,10:N0}|{item.Product.Price,10:N2}|{item.Product.Price * item.Quantity,10:N2}");
            });
            toString.AppendLine(new string('-', 44));

            if (_discounts.Any())
            {
                toString.AppendLine($"{"Discount",44}");
                toString.AppendLine(new string('-', 44));
                toString.AppendLine("  Product | Quantity |  Price   |  Total   |");
                toString.AppendLine(new string('-', 44));
                _discounts.ForEach(discount =>
                {
                    toString.AppendLine($"{discount.Product.Name,10}|{discount.Quantity,10:N0}|{discount.Product.Price,10:N2}|{discount.TotalDiscount,10:N2}|");
                });
                toString.AppendLine(new string('-', 44));
            }
            toString.AppendLine($"Total: {_backetSum,36:N2}");
            toString.AppendLine(new string('-', 44));

            return toString.ToString();
        }

        /// <summary>
        /// Calculate the current balance of basket
        /// </summary>
        private void CalculateBalance()
        {
            // process discounts
            _discounts.Clear();
            _discountServices.ForEach(ds =>
            {
                var d = ds.ProcessDiscount(_items);
                if (d != null)
                {
                    _discounts.Add(d);
                }

            });

            // calculate balance
            _backetSum = _items.Sum(i => i.TotalPrice) - _discounts.Sum(d => d.TotalDiscount);
        }
    }
}