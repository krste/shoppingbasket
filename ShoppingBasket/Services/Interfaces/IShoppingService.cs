﻿using System.Collections.Generic;
using ShoppingBasket.Models.Interfaces;

namespace ShoppingBasket.Services.Interfaces
{
    /// <summary>
    /// Service that handle logic related to shopping basket
    /// </summary>
    public interface IShoppingService
    {
        /// <summary>
        /// Method to add product or increase quantity of that product from shopping basket
        /// </summary>
        /// <param name="product"></param>
        void AddProduct(IBasketItem product);

        /// <summary>
        /// Method to decrease quantity of producti or remove that product from shopping basket
        /// </summary>
        /// <param name="product"></param>
        void RemoveProduct(IBasketItem product);

        /// <summary>
        /// Return list of items in basket
        /// </summary>
        /// <returns></returns>
        ICollection<IBasketItem> GetProducts();

        /// <summary>
        /// Method to add discount logic to shopping basket
        /// </summary>
        /// <param name="discountService">Implementation of <see cref="IDiscountService"/> interface that handle logic for discount</param>
        void AddDiscountService(IDiscountService discountService);

        /// <summary>
        /// Method for removing all discount logic from shopping basket
        /// </summary>
        void RemoveDiscountService(int id);

        /// <summary>
        /// Method for removing all discount logic from shopping basket
        /// </summary>
        void RemoveAllDiscountServices();

        /// <summary>
        /// Return list of discounts for items in basket
        /// </summary>
        /// <returns></returns>
        ICollection<IDiscount> GetDiscounts();

        /// <summary>
        /// Method to retreve balance of shopping basket
        /// </summary>
        /// <returns>decimal balance of shopping bakset</returns>
        decimal GetBasketBalance();
    }
}