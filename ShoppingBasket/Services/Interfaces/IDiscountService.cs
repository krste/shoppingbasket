﻿using System.Collections.Generic;
using ShoppingBasket.Models.Interfaces;

namespace ShoppingBasket.Services.Interfaces
{
    /// <summary>
    /// Interface for Discount servide that do logic of determin discount amount
    /// </summary>
    public interface IDiscountService
    {
        /// <summary>
        /// Unique identifier of discount service
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Process discount logic on list of items from basket
        /// </summary>
        /// <param name="items">List of items in basket</param>
        /// <returns><see cref="IDiscount"/> object containing total discount amount</returns>
        IDiscount ProcessDiscount(ICollection<IBasketItem> items);
    }
}