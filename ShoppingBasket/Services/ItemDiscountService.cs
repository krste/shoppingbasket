﻿using System.Collections.Generic;
using System.Linq;
using ShoppingBasket.Models;
using ShoppingBasket.Models.Interfaces;
using ShoppingBasket.Services.Interfaces;

namespace ShoppingBasket.Services
{
    /// <inheritdoc />
    public class ItemDiscountService : IDiscountService
    {
        private readonly IProduct _item;
        private readonly int _numberOfItems;
        private readonly IProduct _discountItem;
        private readonly decimal _discountShare;

        public int Id { get; }

        /// <summary>
        /// Create new instance of <see cref="ItemDiscountService"/>
        /// </summary>
        /// <param name="id">Unique identifier of discount service</param>
        /// <param name="item">Type of <see cref="IProduct"/> that is condition for discount</param>
        /// <param name="numberOfItems">Number of items of certain <see cref="IProduct"/> that is condition for discount</param>
        /// <param name="discountItem">Type of <see cref="IProduct"/> that will be discounted</param>
        /// <param name="discountShare">Discount percentage displayed as decimal number (20% => 0.2)</param>
        public ItemDiscountService(int id, IProduct item, int numberOfItems, IProduct discountItem, decimal discountShare)
        {
            Id = id;
            _item = item;
            _numberOfItems = numberOfItems;
            _discountItem = discountItem;
            _discountShare = (discountShare > 1) ? 1 : discountShare;
        }

        public IDiscount ProcessDiscount(ICollection<IBasketItem> items)
        {
            // number of items that potentially can be discounted
            var discountedQuantity = 0;

            if (_item.Id == _discountItem.Id)
            {
                // if same product is in condition and on discount then use percent to calculate number of discounted items
                var percent = 1 - (_numberOfItems / ((decimal)_numberOfItems + 1));
                discountedQuantity = (int)((items.FirstOrDefault(i => i.Product.Id == _item.Id)?.Quantity ?? 0) * percent);
            }
            else
            {
                // otehrwise use quantity
                discountedQuantity = (items.FirstOrDefault(i => i.Product.Id == _item.Id)?.Quantity ?? 0) / _numberOfItems;
            }

            // check if there are any item can be discounted
            if (discountedQuantity > 0)
            {
                var countOfDiscountedItems = items.FirstOrDefault(i => i.Product.Id == _discountItem.Id)?.Quantity ?? 0;

                // check if that type of item is in basket
                if (countOfDiscountedItems > 0)
                {
                    // if there are items of type that can be discounted set lower value between existing number of items and number potentially discounted items
                    return new Discount(_discountItem, countOfDiscountedItems > discountedQuantity ? discountedQuantity : countOfDiscountedItems, _discountShare);
                }
            }

            return null;
        }
    }
}