﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShoppingBasket.Models;
using ShoppingBasket.Models.Interfaces;
using ShoppingBasket.Services;
using ShoppingBasket.Services.Interfaces;

namespace ShoppingBasket.Tests
{
    /// <summary>
    /// Tests for <see cref="ItemDiscountService"/> implementation of <see cref="IDiscountService"/>
    /// </summary>
    [TestClass]
    public class ItemDiscountServiceTests
    {
        private readonly Product _butter = new Product(1, (decimal)0.80, "Butter");
        private readonly Product _milk = new Product(2, (decimal)1.15, "Milk");
        private readonly Product _bread = new Product(3, (decimal)1.00, "Bread");

        /// <summary>
        /// No discount
        /// </summary>
        [TestMethod]
        public void NoDiscount()
        {
            var discountShare = new decimal(0.5);

            var service = new ItemDiscountService(1, _butter, 2, _bread, discountShare);
            var discount = service.ProcessDiscount(new List<IBasketItem>()
            {
                new BasketItem(_butter),
                new BasketItem(_bread),
            });

            Assert.AreEqual(null, discount?.TotalDiscount);
        }

        /// <summary>
        /// Discount for one item for different product
        /// </summary>
        [TestMethod]
        public void DifferentItems()
        {
            var discountShare = new decimal(0.5);

            var service = new ItemDiscountService(1, _butter, 2, _bread, discountShare);
            var discount = service.ProcessDiscount(new List<IBasketItem>()
            {
                new BasketItem(_butter, 2),
                new BasketItem(_bread, 2),
            });

            Assert.AreEqual(_bread.Price * discountShare, discount.TotalDiscount);
        }

        /// <summary>
        /// Discount for multiple items for different product
        /// </summary>
        [TestMethod]
        public void MultipleDifferentItems()
        {
            var discountShare = new decimal(0.5);

            var service = new ItemDiscountService(1, _butter, 2, _bread, discountShare);
            var discount = service.ProcessDiscount(new List<IBasketItem>()
            {
                new BasketItem(_butter, 4),
                new BasketItem(_bread, 4),
            });

            Assert.AreEqual(2 * _bread.Price * discountShare, discount.TotalDiscount);
        }

        /// <summary>
        /// Discount for different product when discount criteria is not met
        /// </summary>
        [TestMethod]
        public void DifferentDiscountedItemCriteriaNotMet()
        {
            var discountShare = new decimal(0.5);

            var service = new ItemDiscountService(1, _butter, 2, _bread, discountShare);
            var discount = service.ProcessDiscount(new List<IBasketItem>()
            {
                new BasketItem(_butter, 4)
            });

            Assert.AreEqual(null, discount?.TotalDiscount);
        }

        /// <summary>
        /// Discount for one item for same product
        /// </summary>
        [TestMethod]
        public void SameItem()
        {
            var service = new ItemDiscountService(2, _milk, 3, _milk, 1);
            var discount = service.ProcessDiscount(new List<IBasketItem>()
            {
                new BasketItem(_milk, 4)
            });

            Assert.AreEqual(_milk.Price, discount.TotalDiscount);
        }

        /// <summary>
        /// Discount for multiple items for same product
        /// </summary>
        [TestMethod]
        public void MultipleSameItem()
        {
            var service = new ItemDiscountService(2, _milk, 3, _milk, 1);
            var discount = service.ProcessDiscount(new List<IBasketItem>()
            {
                new BasketItem(_milk, 16)
            });

            Assert.AreEqual(_milk.Price * 4, discount.TotalDiscount);
        }

        /// <summary>
        /// Discount for same product when discount criteria is not met
        /// </summary>
        [TestMethod]
        public void SameDiscountedItemCriteriaNotMet()
        {
            var service = new ItemDiscountService(2, _milk, 3, _milk, 1);
            var discount = service.ProcessDiscount(new List<IBasketItem>()
            {
                new BasketItem(_milk, 3)
            });

            Assert.AreEqual(null, discount?.TotalDiscount);
        }
    }
}