﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShoppingBasket.Models;
using ShoppingBasket.Models.Interfaces;
using ShoppingBasket.Services;
using ShoppingBasket.Services.Interfaces;

namespace ShoppingBasket.Tests
{
    /// <summary>
    /// Test for <see cref="ShoppingService"/> implementation of <see cref="IShoppingService"/>
    /// </summary>
    [TestClass]
    public class ShoppingServiceTests
    {
        private readonly Product _butter = new Product(1, (decimal)0.80, "Butter");
        private readonly Product _milk = new Product(2, (decimal)1.15, "Milk");
        private readonly Product _bread = new Product(3, (decimal)1.00, "Bread");

        /// <summary>
        /// Get balance when there is no items in basket
        /// </summary>
        [TestMethod]
        public void EmptyBasket()
        {
            var shoppingService = new ShoppingService();

            Assert.AreEqual(0, shoppingService.GetBasketBalance());
        }

        /// <summary>
        /// Get balance when one item is added
        /// </summary>
        [TestMethod]
        public void OneItem()
        {
            var shoppingService = new ShoppingService();
            shoppingService.AddProduct(new BasketItem(_butter));

            Assert.AreEqual(_butter.Price, shoppingService.GetBasketBalance());
        }

        /// <summary>
        /// Get balance when producto of same kind is added multiple times
        /// </summary>
        [TestMethod]
        public void AddProductMultipleTimes()
        {
            var shoppingService = new ShoppingService();

            shoppingService.AddProduct(new BasketItem(_butter));
            Assert.AreEqual(_butter.Price, shoppingService.GetBasketBalance());

            shoppingService.AddProduct(new BasketItem(_butter));
            Assert.AreEqual(_butter.Price * 2, shoppingService.GetBasketBalance());
        }

        /// <summary>
        /// Get balance when producto is added and the removed
        /// </summary>
        [TestMethod]
        public void AddRemoveProduct()
        {
            var shoppingService = new ShoppingService();

            shoppingService.AddProduct(new BasketItem(_butter));
            Assert.AreEqual(_butter.Price, shoppingService.GetBasketBalance());

            shoppingService.RemoveProduct(new BasketItem(_butter));
            Assert.AreEqual(0, shoppingService.GetBasketBalance());
        }

        /// <summary>
        /// Get balance when producto is added and more items are removed than bucket contains
        /// </summary>
        [TestMethod]
        public void RemoveMoreItemsThanBucketContains()
        {
            var shoppingService = new ShoppingService();

            shoppingService.AddProduct(new BasketItem(_butter));
            Assert.AreEqual(_butter.Price, shoppingService.GetBasketBalance());

            shoppingService.RemoveProduct(new BasketItem(_butter, 3));
            Assert.AreEqual(0, shoppingService.GetBasketBalance());
        }

        /// <summary>
        /// Get balance when multiple items of same kind are added
        /// </summary>
        [TestMethod]
        public void MultipleItemsOneProduct()
        {
            var numberOfItems = 3;
            var shoppingService = new ShoppingService();
            shoppingService.AddProduct(new BasketItem(_butter, numberOfItems));

            Assert.AreEqual(_butter.Price * numberOfItems, shoppingService.GetBasketBalance());
        }

        /// <summary>
        /// get balance when multiple items of multiple kinds are added
        /// </summary>
        [TestMethod]
        public void MultipleItemsMultipleProducts()
        {
            var shoppingService = new ShoppingService();
            shoppingService.AddProducts(new List<IBasketItem>()
            {
                new BasketItem(_butter,2),
                new BasketItem(_milk, 3),
                new BasketItem(_bread, 4)
            });

            Assert.AreEqual((_butter.Price * 2) + (_milk.Price * 3) + (_bread.Price * 4), shoppingService.GetBasketBalance());
        }

        /// <summary>
        /// Get balance when there is discount logic but condition is not met
        /// </summary>
        [TestMethod]
        public void OneDiscount_CondtionNotMet()
        {
            var shoppingService = new ShoppingService();
            shoppingService.AddProducts(new List<IBasketItem>()
            {
                new BasketItem(_butter),
                new BasketItem(_milk),
                new BasketItem(_bread)
            });

            shoppingService.AddDiscountService(new ItemDiscountService(1, _butter, 2, _bread, new decimal(0.5)));

            Assert.AreEqual(new decimal(2.95), shoppingService.GetBasketBalance());
        }

        /// <summary>
        /// Get balance when there is one discount logic and condition is met
        /// </summary>
        [TestMethod]
        public void OneDiscount_CondtionMet()
        {
            var shoppingService = new ShoppingService();
            shoppingService.AddProducts(new List<IBasketItem>()
            {
                new BasketItem(_butter, 2),
                new BasketItem(_bread, 2)
            });

            shoppingService.AddDiscountService(new ItemDiscountService(1, _butter, 2, _bread, new decimal(0.5)));

            Assert.AreEqual(new decimal(3.1), shoppingService.GetBasketBalance());
        }

        /// <summary>
        /// Get balance when there is multiple discount logic and one condition is met
        /// </summary>
        [TestMethod]
        public void MultipleDiscount_OneCondtionMet()
        {
            var shoppingService = new ShoppingService();
            shoppingService.AddProducts(new List<IBasketItem>()
            {
                new BasketItem(_milk, 4)
            });

            shoppingService.AddDiscountService(new ItemDiscountService(1, _butter, 2, _bread, new decimal(0.5)));
            shoppingService.AddDiscountService(new ItemDiscountService(2, _milk, 3, _milk, 1));

            Assert.AreEqual(new decimal(3.45), shoppingService.GetBasketBalance());
        }

        /// <summary>
        /// Get balance when there is multiple discount logic and multiple conditions are met
        /// </summary>
        [TestMethod]
        public void MultipleDiscount_MultipleCondtionMet()
        {
            var shoppingService = new ShoppingService();
            shoppingService.AddProducts(new List<IBasketItem>()
            {
                new BasketItem(_butter, 2),
                new BasketItem(_bread),
                new BasketItem(_milk, 8)
            });

            shoppingService.AddDiscountService(new ItemDiscountService(1, _butter, 2, _bread, new decimal(0.5)));
            shoppingService.AddDiscountService(new ItemDiscountService(2, _milk, 3, _milk, 1));

            Assert.AreEqual(new decimal(9), shoppingService.GetBasketBalance());
        }
    }
}